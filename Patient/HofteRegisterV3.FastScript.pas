procedure GrayBox( ACheck: TfrxCheckBoxView );
begin
  ACheck.Checked := false;
  ACheck.Frame.BottomLine.Color := clGray;
  ACheck.Frame.TopLine.Color := clGray;
  ACheck.Frame.LeftLine.Color := clGray;
  ACheck.Frame.RightLine.Color := clGray;
  ACheck.Color := $EEDDDD;
end;

procedure RedBox( ACheck: TfrxCheckBoxView );
begin
  ACheck.Frame.BottomLine.Color := clRed;
  ACheck.Frame.TopLine.Color := clRed;
  ACheck.Frame.LeftLine.Color := clRed;
  ACheck.Frame.RightLine.Color := clRed;
  ACheck.Color := $DDDDFF;
end;

function GetOption( const AVarNo: integer ): integer;
var
  varName: string;
  varValue: Variant;
  valueString: string;
begin
  Result := -1;
  varName := Format( 'FORM.VAR%d', [AVarNo] );
  varValue := Get( varName );
  valueString := VarToStr( varValue );
  if ValidInt( valueString ) then
    Result := StrToInt( valueString );
end;

function GetOptionName( const AVarName: string ): integer;
var
  varName: string;
  varValue: Variant;
  valueString: string;
begin
  Result := -1;
  varName := Format( 'FORM.%s', [AVarName] );
  varValue := Get( varName );
  valueString := VarToStr( varValue );
  if ValidInt( valueString ) then
    Result := StrToInt( valueString );
end;

procedure CheckIfOne( const AItemId: integer; ACheck: TfrxCheckBoxView );
var
  ordOption: integer;
begin
  ordOption := GetOption( AItemId );
  ACheck.Checked := ( ordOption = 1 );
  if ACheck.Checked then ACheck.Color := clYellow;
  if ordOption = -1 then RedBox( ACheck );
end;

procedure CheckIfOneName( const AVarName: string; ACheck: TfrxCheckBoxView );
var
  ordOption: integer;
begin
  ordOption := GetOptionName( AVarName );
  ACheck.Checked := ( ordOption = 1 );
  if ACheck.Checked then ACheck.Color := clYellow;
  if ordOption = -1 then RedBox( ACheck );
end;

procedure CheckIfSame( ACheck: TfrxCheckBoxView; const AExpected, AValue2: integer;  );
begin
  ACheck.Checked := ( AExpected = AValue2 );
  if ACheck.Checked then ACheck.Color := clYellow;
  if AValue2 = -1 then RedBox( ACheck );
end;

function IsHemiProtese: boolean;
begin
  Result :=
  ( GetOptionName( 'HrPriOprType' ) = 3 ) or
  ( GetOptionName( 'HrPriOprType' ) = 4 ) or
  ( GetOptionName( 'HrSekHemiBipolar' ) = 1 ) or
  ( GetOptionName( 'HrSekHemiUnipolar' ) = 1 );
end;

procedure Prepare8192;
var
  enumValue: integer;
begin
  { Aktuell operasjon }
  enumValue := GetOptionName( 'HrAktuellOpr' );
  CheckIfSame( cb8192a, 1, enumValue );
  CheckIfSame( cb8192b, 2, enumValue );
end;

procedure Prepare8193;
var
  enumValue: integer;
begin
  { Side }
  enumValue := GetOption( 8193 );
  CheckIfSame( cb8193a, 1, enumValue );
  CheckIfSame( cb8193b, 2, enumValue );
end;

procedure Prepare8199;
var
  enumValue: integer;
begin
  { Tid fra brudd til operasjon }
  enumValue := GetOption( 8199 );
  CheckIfSame( cb8199a, 1, enumValue );
  CheckIfSame( cb8199b, 2, enumValue );
  CheckIfSame( cb8199c, 3, enumValue );
  CheckIfSame( cb8199d, 4, enumValue );
  CheckIfSame( cb8199e, 5, enumValue );
end;

procedure Prepare8200;
var
  enumValue: integer;
begin
  { Demens }
  enumValue := GetOptionName( 'HrDemens' );
  CheckIfSame( cb8200a, 0, enumValue );
  CheckIfSame( cb8200b, 1, enumValue );
  CheckIfSame( cb8200c, 2, enumValue );
end;

procedure Prepare8201;
var
  enumValue: integer;
begin
  { ASA-klasse }
  enumValue := GetOptionName( 'ASA_ANESTESI' );
  CheckIfSame( cb8201a, 1, enumValue );
  CheckIfSame( cb8201b, 2, enumValue );
  CheckIfSame( cb8201c, 3, enumValue );
  CheckIfSame( cb8201d, 4, enumValue );
  CheckIfSame( cb8201e, 5, enumValue );
end;

procedure Prepare8202;
var
  enumValue: integer;
begin
  { Type primærbrudd }
  enumValue := GetOption( 8202 );
  CheckIfSame( cb8202a, 1, enumValue );
  CheckIfSame( cb8202b, 2, enumValue );
  CheckIfSame( cb8202c, 3, enumValue );
  CheckIfSame( cb8202d, 4, enumValue );
  CheckIfSame( cb8202e, 5, enumValue );
  CheckIfSame( cb8202f, 6, enumValue );
  CheckIfSame( cb8202g, 7, enumValue );
  CheckIfSame( cb8202i, 9, enumValue );
end;

procedure Prepare8204;
var
  enumValue: integer;
begin
  { Type primæroperasjon }
  enumValue := GetOptionName( 'HrPriOprType' );
  CheckIfSame( cb8204a, 1, enumValue );
  CheckIfSame( cb8204b, 2, enumValue );
  CheckIfSame( cb8204c, 3, enumValue );
  CheckIfSame( cb8204d, 4, enumValue );
  CheckIfSame( cb8204e, 5, enumValue );
  CheckIfSame( cb8204f, 6, enumValue );
  CheckIfSame( cb8204g, 7, enumValue );
  CheckIfSame( cb8204h, 8, enumValue );
  CheckIfSame( cb8204i, 9, enumValue );
  CheckIfSame( cb8204j, 10, enumValue );
  CheckIfSame( cb8204k, 11, enumValue );
  CheckIfSame( cb8204l, 12, enumValue );
end;

procedure Prepare8230;
var
  enumValue: integer;
begin
  { Fiksasjon av hemiprotese }
  enumValue := GetOptionName( 'HrHemiFiksasjon' );
  CheckIfSame( cb8230a, 1, enumValue );
  CheckIfSame( cb8230b, 2, enumValue );
  CheckIfSame( cb8230c, 3, enumValue );
  mem8231.Visible := enumValue in [2,3];
end;

procedure Prepare8232;
var
  enumValue: integer;
begin
  { Patologisk brudd }
  enumValue := GetOptionName( 'HrPatologiskBrudd' );
  CheckIfSame( cb8232a, 0, enumValue );
  CheckIfSame( cb8232b, 1, enumValue );
end;

procedure Prepare8234;
var
  enumValue: integer;
begin
  { Tilgang til hofteleddet }
  enumValue := GetOptionName( 'HrHemiTilgang' );
  CheckIfSame( cb8234a, 1, enumValue );
  CheckIfSame( cb8234b, 2, enumValue );
  CheckIfSame( cb8234c, 3, enumValue );
  CheckIfSame( cb8234d, 4, enumValue );
  CheckIfSame( cb8234e, 5, enumValue );
end;

procedure Prepare8235;
var
  enumValue: integer;
begin
  { Anestesitype }
  enumValue := GetOption( 8235 );
  CheckIfSame( cb8235a, 1, enumValue );
  CheckIfSame( cb8235b, 2, enumValue );
  CheckIfSame( cb8235c, 3, enumValue );
end;

procedure Prepare8237;
var
  enumValue: integer;
  kompText: string;
begin
  { Komplikasjoner }
  enumValue := GetOption( 8237 );
  CheckIfSame( cb8237a, 0, enumValue );
  CheckIfSame( cb8237b, 1, enumValue );
  kompText := VarToStr( Get( 'FORM.VAR8238' ) );
  Komplikasjoner.Visible := ( enumValue = 1 ) and ( Length( kompText ) > 40 );
end;

procedure Prepare8290;
var
  enumValue: integer;
  prepCount: integer;
begin
  { Antibiotikabehandling }
  enumValue := GetOptionName( 'HrAbProfylakse' );
  CheckIfSame( cb8290a, 0, enumValue );
  CheckIfSame( cb8290b, 1, enumValue );
  { Disable dosing memos based on drug count }
  prepCount := GetOptionName( 'ctrHrAbProfCount' );
  mem8241.Visible := prepCount >= 1;
  mem8485.Visible := prepCount >= 1;
  mem8243.Visible := prepCount >= 1;
  mem8244.Visible := prepCount >= 2;
  mem8486.Visible := prepCount >= 2;
  mem8246.Visible := prepCount >= 2;
  mem8247.Visible := prepCount >= 3;
  mem8487.Visible := prepCount >= 3;
  mem8249.Visible := prepCount >= 3;
end;

procedure Prepare8250;
var
  enumValue: integer;
  prepCount: integer;
begin
  { Antitrombotisk behandling }
  prepCount := GetOptionName( 'ctrHrAtPrepCount' );
  enumValue := GetOptionName( 'HrTrombProfylakse' );
  if prepCount =-1 then
  begin
    RedBox( cb8250a );
    RedBox( cb8250b );
  end
  else
  begin
    CheckIfSame( cb8250a, 0, enumValue );
    CheckIfSame( cb8250b, 1, enumValue );
  end;
  { Disable dosing memos based on drug count }
  mem8251.Visible := prepCount >= 1;
  mem8252.Visible := prepCount >= 1;
  mem8253.Visible := prepCount >= 1;
  mem8635.Visible := prepCount >= 1;
  { Second drug }
  mem8254.Visible := prepCount >= 2;
  mem8256.Visible := prepCount >= 2;
  mem8636.Visible := prepCount >= 2;
end;

procedure Prepare8258;
var
  enumValue: integer;
begin
  { Fibrinolysis }
  enumValue := GetOption( 8258 );
  CheckIfSame( cb8258a, 0, enumValue );
  CheckIfSame( cb8258b, 1, enumValue );
end;

procedure Prepare8261;
var
  enumValue: integer;
begin
  { Surgeon expericence }
  enumValue := GetOptionName( 'HrOprErfaring' );
  CheckIfSame( cb8261a, 0, enumValue );
  CheckIfSame( cb8261b, 1, enumValue );
end;

procedure Prepare8288;
var
  enumValue: integer;
begin
  { First dose of antithrombotics given }
  enumValue := GetOption( 8288 );
  CheckIfSame( cb8288a, 1, enumValue );
  CheckIfSame( cb8288b, 2, enumValue );
end;

procedure Prepare8500;
var
  enumValue: integer;
begin
  { Fast antikoagulasjon }
  enumValue := GetOptionName( 'HrAntiKoag' );
  if enumValue > 0 then enumValue := 1;
  CheckIfSame( cb8500a, 0, enumValue );
  CheckIfSame( cb8500b, 1, enumValue );
end;

procedure Prepare8292;
var
  enumValue: integer;
begin
  { Fiksasjon med eller uten HA }
  enumValue := GetOptionName( 'HrMedHA' );
  CheckIfSame( cb8292a, 1, enumValue );
  CheckIfSame( cb8292b, 2, enumValue );
end;

procedure DisableIrrelevantBoxes;
var
  injuryTimeKnown: boolean;
  primaryOperation: boolean;
begin

  { Static items }
  memPriopSticker.Visible := cb8192a.Checked;
  memReopSticker.Visible := cb8192b.Checked;

  { Some data memos should only be visible if relevant box ticked }
  mem8203.Visible := cb8202g.Checked; // Annen type primærbrudd
  mem8205.Visible := cb8204l.Checked; // Annen type primæroperasjon
  mem8219.Visible := cb8218.Checked;  // Årsak til reoperasjon
  mem8229.Visible := cb8228.Checked;  // Annen type reoperasjon
  mem8233.Visible := cb8232b.Checked; // Type patologisk brudd
  mem8287.Visible := cb8234e.Checked; // Annen tilgang til hofteleddet
  mem8236.Visible := cb8235c.Checked; // Annen type narkose
  mem8238.Visible := cb8237b.Checked; // Komplikasjoner
  mem8259.Visible := cb8258b.Checked; // Fibrinonlysedosering

  { Show timeboxes (and their labels) only for primary operation }
  primaryOperation := cb8192a.Checked;
  mem8196.Visible := primaryOperation;
  mem8197.Visible := primaryOperation;
  infoClockOpTime.Visible := primaryOperation;                                                                                                            
  infoClockFractureTime.Visible := primaryOperation;                                                                                                            
  
  { Decide whether injury time is known }
  mem8196.Text := VarToStr( Get( 'FORM.HrBruddTidspunkt' ) );
  mem8197.Text := VarToStr( Get( 'FORM.HrOprTidspunkt' ) );
  injuryTimeKnown := ( mem8196.Text <>'' ) and ( mem8197.Text <> '' );

  if injuryTimeKnown or ( not primaryOperation ) then
  begin
    { Disable timeframe boxes }
    GrayBox( cb8199a );
    GrayBox( cb8199b );
    GrayBox( cb8199c );
    GrayBox( cb8199d );
    GrayBox( cb8199e );
  end;

  { Disable stuff related to primary operation }
  if not cb8192a.Checked then
  begin
    { This is not a primary operation }
    GrayBox( cb8204a );
    GrayBox( cb8204b );
    GrayBox( cb8204c );
    GrayBox( cb8204d );
    GrayBox( cb8204e );
    GrayBox( cb8204f );
    GrayBox( cb8204g );
    GrayBox( cb8204h );
    GrayBox( cb8204i );
    GrayBox( cb8204j );
    GrayBox( cb8204k );
    GrayBox( cb8204l );
  end;
  { Disable stuff related to reoperation }
  if not cb8192b.Checked then
  begin
    { Disable reason for reoperation }
    GrayBox( cb8206 );
    GrayBox( cb8207 );
    GrayBox( cb8208 );
    GrayBox( cb8209 );
    GrayBox( cb8210 );
    GrayBox( cb8211 );
    GrayBox( cb8212 );
    GrayBox( cb8213 );
    GrayBox( cb8214 );
    GrayBox( cb8215 );
    GrayBox( cb8216 );
    GrayBox( cb8217 );
    GrayBox( cb8218 );
    { Disable type of reoperation }
    GrayBox( cb8220 );
    GrayBox( cb8221 );
    GrayBox( cb8222 );
    GrayBox( cb8223 );
    GrayBox( cb8224 );
    GrayBox( cb8225 );
    GrayBox( cb8226 );
    GrayBox( cb8227 );
    GrayBox( cb8228 );
  end;

  if not IsHemiProtese then
  begin
    GrayBox( cb8292a );
    GrayBox( cb8292b );
    GrayBox( cb8230a );
    GrayBox( cb8230b );
    GrayBox( cb8230c );
    GrayBox( cb8234a );
    GrayBox( cb8234b );
    GrayBox( cb8234c );
    GrayBox( cb8234d );
    GrayBox( cb8234e );
  end;

  { Disable stuff related to first dose of antithrombotics }
  if not cb8250b.Checked then
  begin
    GrayBox( cb8288a );
    GrayBox( cb8288b );
  end;

  { Disable stuff for removing implant }
  if cb8220.Checked then
  begin
    GrayBox( cb8221 );
    GrayBox( cb8222 );
    GrayBox( cb8223 );
    GrayBox( cb8224 );
    GrayBox( cb8225 );
    GrayBox( cb8226 );
    GrayBox( cb8227 );
    GrayBox( cb8228 );
  end;

end;

procedure MasterData2OnBeforePrint(Sender: TfrxComponent);
begin
  Prepare8192; // Primær eller revisjon
  Prepare8193; // Side
  Prepare8199; // Tid fra brudd til operasjon
  Prepare8200; // Kognitiv svikt
  Prepare8201; // ASA-klasse
  Prepare8202; // Type primærbrudd
  Prepare8204; // Type primæroperasjon

  { Årsaker til reoperasjon }

  CheckIfOne( 8206, cb8206 );
  CheckIfOne( 8207, cb8207 );
  CheckIfOne( 8208, cb8208 );
  CheckIfOne( 8209, cb8209 );
  CheckIfOne( 8210, cb8210 );
  CheckIfOne( 8211, cb8211 );
  CheckIfOne( 8212, cb8212 );
  CheckIfOne( 8213, cb8213 );
  CheckIfOne( 8214, cb8214 );
  CheckIfOne( 8215, cb8215 );
  CheckIfOne( 8216, cb8216 );
  CheckIfOne( 8217, cb8217 );
  CheckIfOne( 8218, cb8218 );

  { Type reoperasjon }

  CheckIfOne( 8220, cb8220 );
  CheckIfOne( 8221, cb8221 );
  CheckIfOneName( 'HrSekHemiBipolar', cb8222 );
  CheckIfOneName( 'HrSekHemiUnipolar', cb8223 );
  CheckIfOne( 8224, cb8224 );
  CheckIfOne( 8225, cb8225 );
  CheckIfOne( 8226, cb8226 );
  CheckIfOne( 8227, cb8227 );
  CheckIfOne( 8228, cb8228 );

  Prepare8230; // Fiksasjon av hemiprotese
  Prepare8232; // Patologisk brudd
  Prepare8234; // Tilgang ved hemiprotese
  Prepare8235; // Anestesitype
  Prepare8237; // Peroperative komplikasjoner
  Prepare8250; // Antitrombotisk behandling
  Prepare8258; // Fibrinolytisk behandling
  Prepare8261; // Operatørerfaring
  Prepare8288; // Første dose av antitrombotisk behandling
  Prepare8290; // Antibiotika
  Prepare8292; // Med/uten HA
  Prepare8500; // Fast antikoagulasjon

  DisableIrrelevantBoxes;

end;

procedure Picture1OnBeforePrint(Sender: TfrxComponent);
var
  imageFolder: string;
  fileName: string;
begin
  imageFolder := Get( 'Protocol.Path' );
  fileName := imageFolder + 'Patient\HofteBrudd.jpg';
  // if FileExists( fileName ) then
  Picture1.LoadFromFile( fileName );
end;

procedure mem8239OnBeforePrint(Sender: TfrxComponent);
var
  opTimeStr: string;
begin
  opTimeStr := VarToStr( Get( 'FORM.VAR8239') );
  if ValidInt( opTimeStr ) and ( StrToInt( opTimeStr ) > 0 ) then
    mem8239.Text := opTimeStr
  else
  begin
    mem8239.Text := '(mangler)';
    mem8239.Font.Color := clRed;
    mem8239.Color := $DDDDFF;;
    mem8239.Frame.BottomLine.Color := clRed;
  end;
end;

begin

end.
