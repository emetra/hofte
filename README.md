Om HOFTE
======

Protokollen brukes for registrering av hoftebruddsoperasjoner ved Sykehuset Innlandet.

Mapper
======

* **Documents** - innholder dokumenter som ikke er brukt direkte av systemet.
* **Images** - Inneholder bilder som inngår i dokumentasjon.
* **Patient** - Rapporter (\*.fr3) og oversiktsbilde (\*.html) for en enkeltpasient, samt PDF-filer som brukes av systemet og notatmaler (\*.note).
* **Population** - Rapporter (*.fr3) som inneholder flere pasienter.

Nedlasting
==========

Forvaltere kan laste ned ukentlige oppdateringer av metadata, og kan også få tilsendt en epost med instruksjoner og lenke til nedlasting av zip-fil. 
Ta kontakt med DIPS Brukerstøtte for mer informasjon.


Installasjon
============

Pakk ut nedlastet zip-fil og kopier inn med overskriving av eksisterende filer. 
Kjør deretter `\bin\FastTrakUpdate.exe` og kryss av for alle tabeller før kjøring. 
Husk å krysse av for "Lokale filer".

Bergen, 8. mai 2018.

Magne Rekdal